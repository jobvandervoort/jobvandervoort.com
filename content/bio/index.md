---
title: "Bio"
date: 2019-01-29
draft: false
---

Coming from neuroscience, Job went head-first into tech by starting a startup, later joining GitLab as VP of Product to help scale it from 5 people to a unicorn with 400+ employees. He's now helping the world work remotely as CEO of Remote.com.

In his free time Job loves to spend time with his wife and daughter, and play games and guitar.

![Job](hs.jpg)