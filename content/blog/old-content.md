---
title: "Old Content"
date: 2019-01-05T21:42:15Z
draft: false
---

Missing links to slides or other content previously here? Find it all at [jobv.gitlab.io](http://jobv.gitlab.io).