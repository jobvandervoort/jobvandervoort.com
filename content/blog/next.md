---
title: "I'm leaving GitLab to help everyone work remotely"
date: 2019-01-09
---

**TL;DR: I'm leaving GitLab to help organizations and people work remotely, ultimately better. I'll do that as CEO of Remote.com.**

July last year, my wife Carla was hospitalized. She was pregnant with our daughter, but began an early labor at  32 weeks. A week later, June was born. Healthy, but she would have to stay another three weeks in the hospital before she could come home with us.

The first day in the hospital, I left a message to my colleagues in Slack and didn't worry about work for another two months.
When I did check in with my co-workers, there were no frantic demands for my time - in fact, there was nothing but support and people telling me to take it easy. I was able to prepare for the early arrival of our daughter, while regularly visiting the hospital.

#### Remote companies

I have been working for GitLab for the last five years and helped it grow from 5 to 450 people. We have a grand total of zero offices: everyone works remotely, in over 45 different countries. This time has shown me that remote organizations have a unique advantage: working remotely or distributed forces an organization to do a few things different.

- You can't expect people to work at a particular time, given they're likely to be in a different timezone. Working asynchronously is necessary to function.
- Information can't only exist in the heads of people: the person you need might be asleep! You have to write everything down.

These constraints have a powerful effect on the behavior of an organization. First, it becomes easier to focus on merit, actual work, rather than on how long people stay in the office. In other words: you don't need to count hours to see whether someone is being productive[^1]. You can now allow people to work _whenever_ they want.

Second, as everything is written down, overhead in communication and lack of documentation is significantly reduced. In fact, I'd argue that for this reason alone, remote companies are more efficient[^2] than traditional companies. Everyone can always continue with their work.

Third, you can now hire the best people on the planet in any particular function. Most people don't live in the Bay Area (nor want to), meaning that you can pay people a great salary, and they get to live wherever they want. Many people joining GitLab immediately moved to live near family and friends.

Remote work has massive advantages, but the emergent benefits arguably have an even more significant impact. Zero time to commute. Forced transparency through massive amounts of information available and limited resources to manage that[^3]. Shorter, more focused meetings[^4].

#### The bottom line

We’ve lived our lives by the schedule that our employer sets. We’ve lived in cities where employers have us. And the rest, friends, family, loved ones, hobbies, dreams, come second.

That isn’t necessary. If you work remotely, you can live wherever you want. Work on whatever schedule you want. It allows you to organize your life around the way you want to live, not around what your employer wants. It allows you to live in the middle of the city, or the middle of nowhere.

The future of work means that we can work from wherever we want, whenever we want. It doesn't mean we all work from home - I know many don't want to or can’t. It means we gain significant freedom in making decisions, and having a great ability to prioritize what is most important to us.

This is what I've learned working remotely, and what allowed me to take care of my loved ones when they needed it. Just before I wrote this, I was playing with my daughter and put her to bed. In a bit, I’ll have lunch with my wife. Today’s just another workday.

#### What I'm doing next

Everyone should have this freedom, and I believe I can help. Tomorrow is my last day at GitLab. In two weeks time, I'll be starting as the new CEO at [Remote.com](https://www.remote.com). Together with some awesome people, we will make it easier for organizations and people to work remotely.

We're starting with making it easy to find remote jobs and discover great companies. Once we do that, we'll focus on building tools for companies that make remote work easier.

If you want to work together, send me an email at __job@remote.com__.

If you want to get a message when we're starting and hear what we're doing, sign up to [our occasional newsletter here](http://eepurl.com/gdzmsD).

[^1]: I think that this is true for any good manager, remote or not. But in many companies, spending many hours working still equal being a good worker.
[^2]: https://www.inc.com/scott-mautz/a-2-year-stanford-study-shows-astonishing-productivity-boost-of-working-from-home.html
[^3]: If you have a lot of information to manage, managing access and rights to view and edit that information becomes a task in itself. Defaulting to being more open, transparent, is the path of least resistance here.
[^4]: Meetings at GitLab start _exactly_ at the planned time, and end at the scheduled time. Most meetings last no more than 25 minutes. It’s hard to show up late to a meeting that just requires you to click on a link. Will write about this separately in the future.

Thanks [Bruno](https://twitter.com/brunodagnino), [Brendan](https://twitter.com/olearycrew), [Carla](https://www.carladasilvamatos.com/), [Jeremy](https://twitter.com/gitJeremy) and [Marcelo](https://twitter.com/marcelo_lebre) for proofreading.