---
title: "Basic pasta dough"
date: 2020-04-10
draft: false
---

## Ingredients
- 100g flour to 1 egg
- in our family: at least 100g flour pp

## Make dough

1. Mix ingredients
1. Knead until smooth
1. covered in film, leave in fridge for at least 30 minutes

## Make food

1. Make shapes, use a pasta machine
1. Boil pasta 2-4 minutes in salty water, depending on thickness
