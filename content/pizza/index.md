---
title: "Great pizza dough"
date: 2020-04-09
draft: false
---
Makes 4 pizzas

Oven as hot as it goes

## Dry ingredients
- 7 cups all-purpose flour, plus more for dusting
- 6 tablespoons olive oil
- 1½ tablespoons salt

## Wet ingredients
- 2½ cups (600ml) warm water
- 1 teaspoon sugar
- 2 teaspoons active dried yeast

## Day before

1. Put wet ingredients together, leave for 20 minutes
1. Mix salt with flour
1. Add olive oil and wet mixture to flour, knead until homogeneous
1. *Let rise in room temperature for ~24 hours*

## Pizza day

1. Knead briefly and split dough in four balls
1. Wrap in clean film to store in fridge
1. Take out of the fridge an hour in advance
1. Make pizza, oven as hot as it goes.

adapted from a recipe from [André Silva](https://twitter.com/andreffs18)